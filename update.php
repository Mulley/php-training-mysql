<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ajouter une randonnée</title>
	<link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
</head>
<body>
<?php
$dsn = 'mysql:host=localhost;dbname=reunion_island;port=3306;charset=utf8';
$pdo = new PDO($dsn, 'root', 'pt12MERD');
$getid = $_GET['id'];

//query pour pré remplir les champs

$query = $pdo->query("SELECT * FROM hiking WHERE id = $getid");

$resultat = $query->fetchAll();

foreach ($resultat as $key => $value) {
	$postid = $resultat[$key]['id'];
	$postname = $resultat[$key]['name'];
	$postdifficulty = $resultat[$key]['difficulty'];
	$postdistance = $resultat[$key]['distance'];
	$postduation = $resultat[$key]['duration'];
	$postheight_difference = $resultat[$key]['height_difference'];
}

?>

	<a href="read.php">Liste des données</a>
	<h1>Mise à jour d'une rando</h1>
<form  method="post">
		<div>
			<label for="id">id</label>
			<input type="number" name="id" value="<?=$getid?>" disabled>
		</div>
		<div>
			<label for="name">Name</label>
			<input type="text" name="name" value="<?=$postname?>">
		</div>

		<div>
			<label for="difficulty">Difficulté</label>
			<select name="difficulty">
				<option value="<?=$postdifficulty?>"><?=$postdifficulty?></option>
				<option value="très facile">Très facile</option>
				<option value="facile">Facile</option>
				<option value="moyen">Moyen</option>
				<option value="difficile">Difficile</option>
				<option value="très difficile">Très difficile</option>
			</select>
		</div>
		
		<div>
			<label for="distance">Distance</label>
			<input type="text" name="distance" value="<?=$postdistance?>">
		</div>
		<div>
			<label for="duration">Durée</label>
			<input type="duration" name="duration" value="<?=$postduation?>">
		</div>
		<div>
			<label for="height_difference">Dénivelé</label>
			<input type="text" name="height_difference" value="<?=$postheight_difference?>">
		</div>
		<button type="submit" >Envoyer</button>
</form>

<?php

// modifier une randonnées dans la BDD
$postid = $_POST['id'];
$b = $pdo->prepare("UPDATE hiking SET name = :name, difficulty = :difficulty, distance = :distance, duration = :duration, height_difference = :height_difference WHERE id = $getid");

//? UPDATE table_name
//? SET column1 = 'value1', column2 = 'value2', column3 = 'value3',...
//? WHERE column_name = value

$b->bindParam(':name', $_POST["name"]);
$b->bindParam(':difficulty', $_POST["difficulty"]);
$b->bindParam(':distance', $_POST["distance"]);
$b->bindParam(':duration', $_POST["duration"]);
$b->bindParam(':height_difference', $_POST["height_difference"]);


if ( !empty($_POST["name"]) && !empty($_POST["difficulty"]) && !empty($_POST["distance"]) && !empty($_POST["duration"]) && !empty($_POST["height_difference"]) ) {
	echo "bdd modifiée";
	$b->execute();
// redirection vers la liste
	header('Location: read.php');
  exit();

} else {
	echo "Aucune modification actuellement";
}
	
	
	?>
</body>
</html>