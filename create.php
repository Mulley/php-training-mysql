<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ajouter une randonnée</title>
	<link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
</head>
<body>
	<a href="/read.php">Liste des données</a>
	<h1>Ajouter</h1>
	<form action="create.php" method="GET">
		<div>
			<label for="name">Name</label>
			<input type="text" name="name" value="">
		</div>

		<div>
			<label for="difficulty">Difficulté</label>
			<select name="difficulty">
				<option value="très facile">Très facile</option>
				<option value="facile">Facile</option>
				<option value="moyen">Moyen</option>
				<option value="difficile">Difficile</option>
				<option value="très difficile">Très difficile</option>
			</select>
		</div>
		
		<div>
			<label for="distance">Distance</label>
			<input type="text" name="distance" value="">
		</div>
		<div>
			<label for="duration">Durée</label>
			<input type="" name="duration" value="12:00:01">
		</div>
		<div>
			<label for="height_difference">Dénivelé</label>
			<input type="text" name="height_difference" value="12345">
		</div>
		<button type="submit" name="button">Envoyer</button>
	</form>

<?php

// ajouter une randonnées à la BDD

    $dsn = 'mysql:host=localhost;dbname=reunion_island;port=3306;charset=utf8';
    $pdo = new PDO($dsn, 'root', 'pt12MERD');
	$b = $pdo->prepare("INSERT INTO hiking (name, difficulty, distance, duration, height_difference) VALUES (:name, :difficulty, :distance, :duration, :height_difference)");
//! on compte le nombre d'entrées dans la BDD pour la comparer plus tard
    $query = $pdo->query("SELECT * FROM hiking ORDER BY id DESC");
	$resultat = $query->fetchAll();
	$nb_id = count($resultat);

	echo " nb id avant envoie du formulaire : ".$nb_id."<br>"; // on est pas obliger d'afficher ça ...


if ( !empty($_GET["name"]) && !empty($_GET["difficulty"]) && !empty($_GET["distance"]) && !empty($_GET["duration"]) && !empty($_GET["height_difference"])) {
	echo "tout les champs sont plein<br>";
	
	$b->bindParam(':name', $_GET["name"]);
	$b->bindParam(':difficulty', $_GET["difficulty"]);
	$b->bindParam(':distance', $_GET["distance"]);
	$b->bindParam(':duration', $_GET["duration"]);
	$b->bindParam(':height_difference', $_GET["height_difference"]);
	$b->execute();
	
//! on REcompte le nombre d'entrées dans la BDD
	$query = $pdo->query("SELECT * FROM hiking ORDER BY id DESC");
	$resultat = $query->fetchAll();
	$nb_id2 = count($resultat);
//! Puis on compare avant et après pour vérifier qu'il y ai bien eu une entréé en BDD
	if ($nb_id2 > $nb_id) {
		echo" nb id2 : ".$nb_id2." il y a bien eu une entrée en BDD! (✿◠‿◠)<br>";
	} elseif ($nb_id == $nb_id2) {
		echo "Il n'y a pas eu d'entrée en BDD !  (▀̿̿Ĺ̯̿▀̿ ̿)";
	} else {
		echo "cet echo ne devrait jamais s'afficher ಠ_ಠ";
	}
	
} else {
	echo "tout les champs ne sont pas remplis";
}



?>


</body>
</html>