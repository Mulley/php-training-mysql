<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Randonnées</title>
  <link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
</head>

<body>
<a href="create.php">ajout d'une rando</a>
<h1>Liste des randonnées</h1>

<?php

// Afficher la liste des randonnées

    $dsn = 'mysql:host=localhost;dbname=reunion_island;port=3306;charset=utf8';
    $pdo = new PDO($dsn, 'root', 'pt12MERD');

    echo "<h3>Afficher toutes les randos.</h3><p>Cliquez sur les noms pour modifier.</p>";
    $query = $pdo->query("SELECT * FROM hiking");
    $resultat = $query->fetchAll();
?>

<table border="1">
  <tr>
      <td>ID</td>
      <td>Nom</td>
      <td>Difficulté</td>
      <td>Distance</td>
      <td>Durée</td>
      <td>Dénivelé</td>
  </tr>

<?PHP

foreach ($resultat as $key => $value) {
  echo "<tr>";
  echo "<td>" . $resultat[$key]['id'] . "</td>";
  echo "<td><a href=\"update.php?id=".$resultat[$key]['id']."\">" . $resultat[$key]['name'] ."</a></td>";
  echo "<td>" . $resultat[$key]['difficulty'] . "</td>";
  echo "<td>" . $resultat[$key]['distance'] . "</td>";
  echo "<td>" . $resultat[$key]['duration'] . "</td>";
  echo "<td>" . $resultat[$key]['height_difference'] . "</td>";
  echo "</tr>";
}

?>
</table>

</body>

</html>
